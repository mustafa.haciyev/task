package com.example.newcont.controller;


import com.example.newcont.model.Teacher;
import com.example.newcont.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/teacher")
@RequiredArgsConstructor
public class TeacherController {

    private final TeacherService teacherService;

    @GetMapping("/{id}")
    public Teacher getTeacher(@PathVariable Long id) {
        return teacherService.getTeacher(id);
    }

    @PostMapping
    public Teacher postTeacher(@RequestBody Teacher teacher) {
        return teacherService.postTeacher(teacher);
    }
//    @PostMapping
//    public String postTeacher(@RequestBody Teacher teacher) {
//        teacherService.postTeacher(teacher)
//        return "Student insert : " + teacher.getName();
//    }


    @PutMapping("{id}")
    public Long putTeacher(@PathVariable Long id, @RequestBody Teacher teacher) {
        return teacherService.putTeacher(id,teacher);
    }

    @DeleteMapping("/{id}")
    public String deleteTeacher(@PathVariable Long id) {
        teacherService.deleteTeacher(id);
        return "Deleted tc id : " + id;
    }


}
