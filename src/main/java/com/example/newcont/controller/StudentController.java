package com.example.newcont.controller;


import com.example.newcont.model.Student;
import com.example.newcont.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final ModelMapper modelMapper;

    private final StudentService studentService;

    @GetMapping("/{id}")
    public Student getStudentById(@PathVariable int id) {
        return studentService.getStudentById(id);
    }

    //    @PostMapping
//    public Student saveStudent(@RequestBody Student student) {
//        return studentService.saveStudent(student);
//    }
    
    @PostMapping
    public String saveStudent(@RequestBody Student student) {
        studentService.saveStudent(student);
        return "Student insert : " + student.getName();
    }


    @PutMapping("/{id}")
    public Integer updateStudent(@RequestBody Student student,@PathVariable int id) {
        return studentService.updateStudent(id, student);
    }

    @DeleteMapping
    public String deleteStudent(@RequestParam(value = "id") int id) {
        studentService.deleteStudent(id);
        return "Deleted id succesfully: " + id;
    }

//    @DeleteMapping("/{id}")
//    public String deleteStudent(@PathVariable int id){
//        studentService.deleteStudent(id);
//        return "Deleted id succesfully: " + id;


}
