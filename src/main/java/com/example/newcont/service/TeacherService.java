package com.example.newcont.service;

import com.example.newcont.model.Teacher;


public interface TeacherService {

    Teacher getTeacher(Long id);

    Teacher postTeacher(Teacher teacher);
//    String postTeacher(Teacher teacher);

    Long putTeacher(Long id, Teacher teacher);

    void deleteTeacher(Long id);
}
