package com.example.newcont.service;

import com.example.newcont.model.Student;


public interface StudentService {


     Student getStudentById(int id);
//     Student saveStudent(Student student);
     String saveStudent(Student student);
     Integer updateStudent(int id, Student student);
     void deleteStudent(int id);
}
