package com.example.newcont.service.impl;

import com.example.newcont.model.Teacher;
import com.example.newcont.repository.TeacherRepository;
import com.example.newcont.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TeacherServiceİmpl implements TeacherService {

    private final TeacherRepository teacherRepository;

    @Override
    public Teacher getTeacher(Long id) {
        return teacherRepository.findById(id).get();
    }

    @Override
    public Teacher postTeacher(Teacher teacher) {
        return teacherRepository.save(teacher);
    }
//    @Override
//    public String postTeacher(Teacher teacher) {
//        return "Student : " + teacherRepository.save(teacher);
//    }
    @Override
    public Long putTeacher(Long id, Teacher teacher) {
        if (teacherRepository.findById(id).isPresent()){
            Teacher teacher1 = teacherRepository.findById(id).get();
            teacher1.setName(teacher.getName());
            teacher1.setSurname(teacher.getSurname());

            teacherRepository.save(teacher1);
        } else {
            throw new RuntimeException("User not found with : " + id + " id");
        }
        return id;
    }

    @Override
    public void deleteTeacher(Long id) {
        teacherRepository.deleteById(id);
    }
}
