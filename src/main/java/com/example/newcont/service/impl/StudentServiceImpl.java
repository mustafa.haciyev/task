package com.example.newcont.service.impl;

import com.example.newcont.model.Student;
import com.example.newcont.repository.StudentRepository;
import com.example.newcont.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.management.RuntimeMBeanException;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
private final StudentRepository studentRepository;
    @Override
    public Student getStudentById(int id) {
        return studentRepository.findById(id).get();
    }

    @Override
    public String saveStudent(Student student) {
        return "Student" +  studentRepository.save(student);
    }

    @Override
    public Integer updateStudent(int id, Student student) {
        if (studentRepository.findById(id).isPresent()){
            Student student1 = studentRepository.findById(id).get();
                    student1.setName(student.getName());
                    student1.setSurname(student.getSurname());
                    student1.setAge(student.getAge());
                    studentRepository.save(student1);
                } else {
            throw new RuntimeException("User not found with : " + id + " id");
        }
        return id;

    }




    @Override
    public void deleteStudent(int id) {
        studentRepository.deleteById(id);
    }
}
