package com.example.newcont;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class NewcontApplication{

//	private final StudentService studentServiceImpl;
//	private final StudentController studentController;
//	private final ModelMapper modelMapper;
//	private final Properties properties;
//
////	@Value("${student.name}")
//	private String name;

	public static void main(String[] args) {
		SpringApplication.run(NewcontApplication.class, args);
	}


//	@Override
//	public void run(String... args) throws Exception {
//		studentServiceImpl.saveStudent(new Student(1,"Mustafa", "Haciyev",25));
//		log.error("Student name is : {} ",name);
//		for (String name : properties.getName()){
//			System.out.println(name);
//		}
//		System.err.println("Age :" + properties.getAge());
//		System.err.println("Surname: " + properties.getSurname());
//	}
}
