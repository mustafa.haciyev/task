FROM openjdk:17
COPY build/libs/newcont-0.0.1-SNAPSHOT.jar /ms18-app/
CMD ["java", "-jar","/ms18-app/newcont-0.0.1-SNAPSHOT.jar"]